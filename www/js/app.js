/**
 * Created by florian on 26/06/2015.
 */
document.addEventListener("pageinit", function(e) {
    if (e.target.id == "accueil") {
        var user_name, api_key;
        user_name = "gl_alexclick";
        //user_name = "gaminglive_tv1";
        api_key = "5j0r5b7qb7kro03fvka3o8kbq262wwm";

        $.getJSON('https://api.twitch.tv/kraken/streams/' + user_name + '?client_id=' + api_key + '&callback=?', function (data) {
            if (data.stream) {
                document.getElementById("accueil_statut_live").innerHTML = "En ligne";
                document.getElementById("accueil_statut_live").style.color = "green";
                document.getElementById("accueil_jeux_live").innerHTML = data.stream.game;
                document.getElementById("accueil_viewers_live").innerHTML = "Rejoindre les " + data.stream.viewers + " viewers !";
            } else {
                document.getElementById("accueil_statut_live").innerHTML = "Hors ligne";
                document.getElementById("accueil_statut_live").style.color = "red";
                document.getElementById("accueil_jeux_live").innerHTML = "";
                document.getElementById("accueil_viewers_live").innerHTML = "";
            }
        });
    }
    if (e.target.id == "planning") {
    }
    if (e.target.id == "equipe") {
        $( ".equipe_img_click" ).click(function() {
            menu.setMainPage('equipePerso.html', {closeMenu: true})
        });
    }
    if (e.target.id == "equipePerso") {
        $( ".equipe_img_click" ).click(function() {
        });
    }
    if (e.target.id == "live") {
        var height = window.innerHeight;
        var offset = 44;
        height = height - offset;
        $("#iframeLive").attr('src',"http://www.twitch.tv/gl_alexclick/embed");
        $("#iframeLive").load(function () {
            $("#iframeLive").height(height);

        });
        $(window).resize(function() {
            height = window.innerHeight;
            offset = 44;
            height = height - offset;
            $("#iframeLive").height(height);
        });
    }
}, false);